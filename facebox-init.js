$(document).ready(function($) {
  $('a[rel*=facebox]').facebox({
    loadingImage: Drupal.settings.facebox_path + '/library/loading.gif',
    closeImage: Drupal.settings.facebox_path + '/library/closelabel.png'
  })
}) 