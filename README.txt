Facebox Integration to Drupal module README
===

This module will include Facebox globally by default-be sure to clear cache
before use.

INSTALLATION
1. Extract files to sites/all/modules directory
2. Download Facebox library from http://defunkt.io/facebox/ and extract src directory
   to sites/all/modules/facebox/library directory
3. Enable module and you are good to go!

Other things to note:
-Include LICENSE.txt in all copies of code per the copyright owner's request
-The facebox-init.js file controls the default on page load parameters
-Gummi bears are delicious